#!/usr/bin/env oksh

######
## KornShell completions. Not complete, just my most commonly used
## References via ~/.kshrc
##
## Inspired by:
## https://github.com/qbit/dotfiles/blob/master/common/dot_ksh_completions
## https://deftly.net/posts/2017-05-01-openbsd-ksh-tab-complete.html
##
## Golden rule: Don't Autocomplete Destructive Operations!

## Cross-platform
set -A complete_git     -- blame diff log main origin pull push rebase status trunk
set -A complete_make    -- config config-recursive clean distclean deinstall install missing reinstalal rmconfig \
                           showconfig
set -A complete_pkgin   -- autoremove avail clean full-upgrade install keep list search update upgrade
set -A complete_service -- onereload onerestart onestop restart start status stop
set -A complete_stanley -- bucket escapepod leftdoor lounge rightdoor unplug
set -A complete_svn     -- add auth ci cleanup co cp export info list lock log merge mv revert sw up upgrade
set -A complete_zfs     -- clone create diff get jail list mount promote receive set send share snapshot upgrade
set -A complete_zpool   -- add attach checkpoint clear create detach export get history import iostat list offline \
                           online scrub set split status upgrade

## Technical writing
set -A complete_hugo   -- check config convert env gen help import list new server version
set -A complete_mkdocs -- build new serve
set -A complete_latex
set -A complete_magick -- convert identify mogrify -autolevel -chop -normalize +repage -resize -quality

## Remote access
_SSH_HOSTS=$(awk '/^Host/{ print $2 }' ~/.ssh/config)
set -A complete_mosh -- $_SSH_HOSTS
set -A complete_scp  -- $_SSH_HOSTS
set -A complete_sftp -- $_SSH_HOSTS
set -A complete_ssh  -- $_SSH_HOSTS

case $(uname) in

	Darwin)
		set -A complete_brew      -- clean install list remove search update upgrade
		set -A complete_diskutil  -- activity addVolume convert create createContainer decrypt eject encrypt info list \
		                             lock mount mountDisk partitionDisk passwd quota revert reserve repairDisk \
                                     repairVolume unmount unmountDisk verifyDisk verifyVolume
		set -A complete_hdiutil   -- attach checksum compact convert create detach info unmount verify
		set -A complete_launchctl -- attach blame bootstrap debug enable error examine kickstart kill plist print reboot
		;;

	FreeBSD)
		set -A complete_geli  -- attach backup configure label list restore unload
		set -A complete_gnop  -- create configure list reload status unload
		set -A complete_gpart -- add bootcode create resize status
		set -A complete_pkg   -- audit autoremove clean info install lock search unlock update upgrade
		;;

	Linux)
		set -A complete_apt-cache -- search
		set -A complete_apt-get   -- autoremove clean dist-upgrade install remove update upgrade
		set -A complete_dnf       -- autoremove clean distro-sync install list provides repolist search update upgrade
		set -A complete_systemctl -- reload start status service stop
		;;

	NetBSD)
		set -A complete_dkctl -- addwedge getwedgeinfo listwedges synccache
		set -A complete_gpt   -- add biosboot create label migrate recover set show type
		;;

esac

## EOF
