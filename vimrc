""""""
"" Ruben's 2022 .vimrc

"" Basic UI
syntax on
set hlsearch
set number
set ruler
set showmode

"" Reading and writing files
set encoding=utf-8

"" Disable arrow keys: old habits die hard
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

"" Hard tab indentation, 4 visual spaces
set autoindent
set shiftwidth=4
set smartindent
set tabstop=4
set wrap linebreak

"" Detect OPML as XML
autocmd BufNewFile,BufRead *.opml set filetype=xml

"" Folding in markdown files
""let g:markdown_folding=1

"" Enable spell checking on text files and Markdown
autocmd FileType txt,markdown setlocal spell complete+=kspell
set spelllang=en_gb

"" Backspace All The Things™ in insert mode
set backspace=indent,eol,start

"" This is so cool! Command autocompletion
set wildmenu
set wildmode=list:full

"" Vim-plug plugins
call plug#begin()
Plug 'jsit/toast.vim'
Plug 'SirVer/ultisnips'
call plug#end()

"" GVim for FreeBSD, NetBSD, and Linux
if has('gui_running')
    set background=light
	set columns=84 "" 80 + line numbers
	set lines=48
	set guifont=Liberation\ Mono\ Regular\ 10
	"" Test for Kanji, Katakana, Hiragana, Hangul, Simplified, Emoji
	"" 漢字 カタカナ ひらがな 한글 汉字 🔰
	set guifontwide=IPAGothic
	set guioptions-=T
	set linespace=2
    set termguicolors
	colorscheme toast
endif

"" MacOS doesn't like the above guifont
if has('gui_macvim')
	set guifont=Liberation\ Mono:h12
endif

"" Utilsnips (evaluating alternatives)
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

"" EOF
