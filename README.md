# dotfiles-desu

This is my personal dotfiles stash. New orchestration scripts and related tools
now belong in my [lunchbox].

I've changed my toolchain significantly since 2012, so much of this repo is now
<span style="text-decoration:line-through;">hysterical</span> historical. I now use the *excellent* [portable OpenBSD
Kornshell] as my daily driver, so those associated scripts will be the most
current. Mmm, currents.

This was also my first public repo, and taught me git. I still prefer svn, but
I feel that ship has now sailed.

[lunchbox]: https://codeberg.org/rubenerd/lunchbox
[portable OpenBSD Kornshell]: https://github.com/ibara/oksh

