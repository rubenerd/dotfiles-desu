#!/usr/bin/env oksh

######
## .kshrc for oksh(1), the OpenBSD public domain kornshell
## Ruben Schade
## List of tools I use: <https://rubenerd.com/omake.opml>

set -o allexport

BLOCKSIZE=M
CLICOLOR=1
EDITOR=vim
HOST=$(hostname)
LANG="en_AU.UTF-8"
LC_CTYPE="$LANG"
LC_PAPER="a4"
MOZ_CRASHREPORTER_DISABLE=1
PS1="$(tput bold)\n\u@${HOST}:\w\n\$$(tput sgr0) "
VISUAL=gmacs

## Paths
MANPATH="/opt/pkg/man:/usr/local/man:$MANPATH"
PATH="$HOME/.local/bin:/opt/pkg/bin:/opt/pkg/sbin:$PATH"
[ -d $HOME/Repos/mugboss/bin ] && PATH="$HOME/Repos/mugboss/bin:$PATH"

## Programs
alias aria2c="aria2c --file-allocation=none"
alias ap="ansible-playbook"
alias cls="tput clear"
alias flac="flac --preserve-modtime --keep-foreign-metadata --best --delete-input-file"
alias lame="lame -b 320 -q 0"
alias magick="magick -density 144x144"
alias mkisodos='mkisofs -rational-rock -joliet -iso-level 1 -input-charset "cp437"'
alias mkisofs="mkisofs -joliet -rational-rock"
alias pnc="pngcrush -blacken -check -fix -oldtimestamp -ow -reduce -speed -v"
alias plzip="plzip --best --verbose --verbose"
alias rsync="rsync -rvtl --human-readable --progress --xattrs"
alias tiled="montage -mode concatenate -tile 1x"
alias yta="yt-dlp -S ext:mp4:m4a -o '%(upload_date)s - %(creator)s - %(title)s - %(ext)s' --mtime --merge-output-format mp4 --embed-thumbnail --embed-metadata --embed-chapters --xattrs --sponsorblock-mark all"
alias ytv="yt-dlp -f bestvideo --no-embed-metadata --no-embed-subs --no-embed-thumbnail --no-sponsorblock"

## I like verbosity
alias cp="cp -v"
alias mkdir="mkdir -v"
alias mv="mv -v"
alias ln="ln -v"
alias rmdir="rmdir -v"
alias tar="tar -v"

## HP-UX style ls(1)
alias ls="ls -F"                              ## standard list
alias la="ls -AF"                             ## list all
alias ll="ls -AFlh -D '%Y-%m-%d %H:%M:%S'"    ## long list, ISO date format
alias lt="ls -AFlhrt -D ''%Y-%m-%d %H:%M:%S'" ## long list, reverse-sorted by mod date

## Muscle memory and OS-specific stuff
case $(uname) in

	Darwin)
		## Homebrew for Java and graphical software; pkgsrc for everything else
		PATH="/opt/homebrew/bin:/opt/homebrew/sbin:/opt/homebrew/opt/openjdk@17/bin:$PATH"
		eval "$(/opt/homebrew/bin/brew shellenv)"
		;;

	FreeBSD)
		alias pkgng="pkg"
		;;

	illumos)
		;;

	Linux)
		command -v dnf &> /dev/null && alias yum="dnf" ## old habits die hard 

		alias ls="ls -F --color=auto"
		alias ls="ls -AF --color=auto"
		alias ls="ls -AFlh --color=auto"
		alias ls="ls -AFlhrt --color=auto"
		export TIME_STYLE=long-iso
		;;
	
	NetBSD)
		## Password is anoncvs, as per <https://www.netbsd.org/mirrors/#anoncvs-japan>
		CVSROOT=":pserver:anoncvs@anoncvs.jp.NetBSD.org:/cvs/cvsroot"
		CVS_RSH="ssh"

		if command -v colorls &> /dev/null; then
			alias ls="colorls -FG"
			alias la="colorls -AFG"
			alias ll="colorls -AFGlh"
			alias lt="colorls -AFGlhrt"
		fi
		;;
esac

[ -f ~/.oksh_completions.sh ] && . ~/.oksh_completions.sh

set +o allexport

