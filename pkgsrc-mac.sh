#!/bin/sh

######
## pkgsrc for Mac, to replace Homebrew
## 2020-06

_PYTHON="39"

## Utilities
pkgin install ansible aria2 arping cdrtools colordiff colorize dcfldd    \
    ddrescue entr fortune git htop httping ioping lzip lzop mdf2iso mosh \
    mtr ncftp nmap nvi oksh openssl ossp-uuid patch ruby rsync sipcalc   \
	sqlite3 srm subversion tcpdump tcpdstat testdisk the_silver_searcher \
	tree unrar unzip watch wtf

## Essential Perl
pkgin install lang/perl5       \
	devel/p5-IO-Async          \
	devel/p5-Moose             \
	graphics/p5-Image-ExifTool \
	graphics/p5-PerlMagick     \
	textproc/p5-Text-Template  \
	textproc/p5-XML-LibXML

## Other dev stuff
pkgin install lang/python${_PYTHON} lang/ruby

## Console
pkgin install dialog irssi links minicom ncurses oksh tmux vim

## Documentation and writing
pkgin install chktex cmark docbook docbook-website docx2txt go-hugo \
	hunspell hunspell-en_GB py${_PYTHON}-mkdocs tesseract           \
	texlive-collection-latex texlive-collection-latex-doc wordnet

## Podcasting and media
pkgin install cmark cowsay eyed3 ffmpeg4 figlet figlet-fonts flac \
	gifsicle ImageMagick lame mkvtoolnix neofetch normalize       \
	pngcrush net/yt-dlp

## Fonts
pkgin install Code-New-Roman dejavu-ttf inconsolata-g sourcesans-fonts \
    sourcehansans-fonts

## Install fonts (requires manual step)
mkdir /Library/Fonts/pkgsrc
cp /opt/pkg/share/fonts/X11/OTF/* /Library/Fonts/pkgsrc/
cp /opt/pkg/share/fonts/X11/TTF/* /Library/Fonts/pkgsrc/

## Help out
pkgin install pkgsurvey
/opt/pkg/bin/pkgsurvey

